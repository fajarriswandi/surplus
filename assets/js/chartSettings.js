
//==============================================
// Revenue Chart
//==============================================
var ctx = document.getElementById("revenueChart").getContext('2d');
                      var myChart = new Chart(ctx, {
                          type: 'line',
                          responsive: true,
                          data: {
                              labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des"],
                              datasets: [{
                                  label: ' Month',
                                  data: [12, 19, 5, 5, 8, 3,6,8,4,9,6,20],
                                  backgroundColor: [
                                      'rgba(83, 163, 255, 0.2)',
                                      'rgba(50, 50, 235, 1',
                                      'rgba(255, 255, 85, 1)',
                                      'rgba(75, 192, 192, 1)',
                                      'rgba(153, 102, 255, 1)',
                                      'rgba(255, 159, 64, 1)'
                                  ],
                                  borderColor: [
                                      'rgba(83, 163, 255,1)',
                                      'rgba(54, 162, 235, 1)',
                                      'rgba(255, 206, 86, 1)',
                                      'rgba(75, 192, 192, 1)',
                                      'rgba(153, 102, 255, 1)',
                                      'rgba(255, 159, 64, 1)'
                                  ],
                                  borderWidth: 2
                              }]
                          },
                          options: {
                            bezierCurve : false,
                              scales: {
                                  yAxes: [{
                                      ticks: {
                                          beginAtZero:true,
                                          fontFamily: "Segoe UI",
                                          fontColor:'rgb(161, 161, 161)',
                                          fontSize: 12
                                      },
                                  }],
                                  responsive: true,
                                  xAxes: [{
                                      ticks: {
                                          beginAtZero:true,
                                          fontFamily: "Segoe UI",
                                          fontColor:'rgb(161, 161, 161)',
                                          fontSize: 12
                                      },
                                      gridLines: {
                                        display: false,
                                      }
                                  }]
                              },
                              legend: {
                              labels: {
                                  // This more specific font property overrides the global property
                                  fontSize: 8,
                                  fontFamily:'Segoe UI',
                                  fontColor: 'rgb(161, 161, 161)'
                              }
                          },
                          
                          }
                      });
//==============================================
// Impression Chart
//==============================================
var ctx = document.getElementById("impressionChart").getContext('2d');
                  var myChart = new Chart(ctx, {
                      type: 'bar',
                      responsive: true,
                      data: {
                          labels: ["Jan", "Feb", "Mar", "Apr", "Mei"],
                          datasets: [{
                              label: ' Month',
                              data: [12, 19, 5, 5, 8],
                              backgroundColor: [
                              'rgba(83, 163, 255, 1)',
                              'rgba(83, 163, 255, 1)',
                              'rgba(83, 163, 255, 1)',
                              'rgba(83, 163, 255, 1)',
                              'rgba(83, 163, 255, 1)',
                              'rgba(83, 163, 255, 1)'
                              ],
                              borderColor: [
                                'rgba(83, 163, 255, 1)',
                                'rgba(83, 163, 255, 1)',
                                'rgba(83, 163, 255, 1)',
                                'rgba(83, 163, 255, 1)',
                                'rgba(83, 163, 255, 1)',
                                'rgba(83, 163, 255, 1)'
                              ],
                              
                              borderWidth: 2
                          }]
                      },
                      options: {
                        bezierCurve : false,
                          scales: {
                              yAxes: [{
                                  ticks: {
                                      beginAtZero:true,
                                      fontFamily: "Segoe UI",
                                      fontColor:'rgb(161, 161, 161)',
                                      fontSize: 12
                                  },
                              }],
                              xAxes: [{
                                  ticks: {
                                      beginAtZero:true,
                                      fontFamily: "Segoe UI",
                                      fontColor:'rgb(161, 161, 161)',
                                      fontSize: 12
                                  },
                                  gridLines: {
                                    display: false,
                                  }
                              }]
                          },
                          legend: {
                          labels: {
                              // This more specific font property overrides the global property
                              fontSize: 8,
                              fontFamily:'Segoe UI',
                              fontColor: 'rgb(161, 161, 161)'
                          }
                      },
                      
                      }
									});


