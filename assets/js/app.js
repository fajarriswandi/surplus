var $header = $('header'),
    $headerHeight = $header.height(),
    $title = $('.title');

var navScroll = {
    
  init:function(){
    $(window).on('scroll',function(){
      navScroll.navDrop();
    })
  },
  
  navDrop:function(){
    var $scrollTop = $(window).scrollTop();
    
    if($scrollTop > $headerHeight){
      $header.addClass('scrolled'); 
      $title.css('padding-top','70px');
    }
    else if($scrollTop == 0) {
      $header.removeClass('scrolled');
    }
    
  }
}

$(document).ready(function(){
  navScroll.init();
})
// ============================================
// Modal Video Header Homepage
// ============================================
$("#modalVideo").on('hidden.bs.modal', function (e) {
	$("#modalVideo iframe").attr("src", $("#modalVideo iframe").attr("src"));
});

// ============================================
// Mobile Menu
// ============================================
function openNav() {
	document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
	document.getElementById("myNav").style.width = "0%";
}


// ============================================
// Smooth Scroll 
// ============================================
$(function() {
	var $window = $(window), $document = $(document),
		transitionSupported = typeof document.body.style.transitionProperty === "string", 
		scrollTime = 1; 

	$(document).on("click", "a[href*=#]:not([href=#])", function(e) {
		var target, avail, scroll, deltaScroll;
    
		if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
			target = $(this.hash);
			target = target.length ? target : $("[id=" + this.hash.slice(1) + "]");

			if (target.length) {
				avail = $document.height() - $window.height();

				if (avail > 0) {
					scroll = target.offset().top;
          
					if (scroll > avail) {
						scroll = avail;
					}
				} else {
					scroll = 0;
				}

				deltaScroll = $window.scrollTop() - scroll;

				if (!deltaScroll) {
					return;
				}

				e.preventDefault();
				
				if (transitionSupported) {
					$("html").css({
						"margin-top": deltaScroll + "px",
						"transition": scrollTime + "s ease-in-out"
					}).data("transitioning", scroll);
				} else {
					$("html, body").stop(true, true) 
					.animate({
						scrollTop: scroll + "px"
					}, scrollTime * 1000);
					
					return;
				}
			}
		}
	});

	if (transitionSupported) {
		$("html").on("transitionend webkitTransitionEnd msTransitionEnd oTransitionEnd", function(e) {
			var $this = $(this),
				scroll = $this.data("transitioning");
			
			if (e.target === e.currentTarget && scroll) {
				$this.removeAttr("style").removeData("transitioning");
				
				$("html, body").scrollTop(scroll);
			}
		});
	}
});

// Hide subnav hompage at scroll down
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("mainMenu").style.top = "0px";
  } else {
    document.getElementById("mainMenu").style.top = "-80px";
  }
  prevScrollpos = currentScrollPos;
}


// ============================================
// Button Back
// ============================================
function goBack() {
	window.history.back();
}
// ============================================
// Popovers WHY WE EXIST
// ============================================
$(function () {
  $('.popOverExist').popover({
		container: 'body',
		html: true,
		toggle: 'popover',
		trigger:'hover' 
  })
})

// ============================================
// Blink 
// ============================================
document.addEventListener('click', click);

function click(e) {
  let el
  
  el = e.target
  
  if (el !== e.currentTarget) {
    if(el.nodeName === 'BUTTON') {
      
       if(el.classList.contains('is-active')) {
         el.classList.remove('is-active')
       } else {
         el.classList.add('is-active')
       }
    }
  }
  event.stopPropagation()
}
// ============================================
// Sidebar Nav 
/** 
 * function openNav() {
	var element = document.getElementById("mySidenav");
	var partial = document.getElementById("nav-partial");
	element.style.width = "180px";
	element.classList.add("navTransition");
	partial.classList.remove("navTransitionOut");
}

function closeNav() {
	var element = document.getElementById("mySidenav");
	var partial = document.getElementById("nav-partial");
	element.style.width = "0";
	element.classList.remove("navTransition");
	partial.classList.add("navTransitionOut");
}

 * 
*/
// ============================================


/*

function openNav() {
	var element = document.getElementById("mySidenav");
	var partial = document.getElementById("nav-partial");
	element.style.width = "180px";
	document.getElementById("main").style.marginLeft = "180px";
	element.classList.add("navTransition");
	partial.classList.remove("navTransitionOut");
	//document.body.style.backgroundColor = "rgba(0,0,0,0.9)";
}

function closeNav() {
var element = document.getElementById("mySidenav");
var partial = document.getElementById("nav-partial");
	document.getElementById("mySidenav").style.width = "0";
	document.getElementById("main").style.marginLeft= "60px";
	element.classList.remove("navTransition");
	partial.classList.add("navTransitionOut");
	//document.body.style.backgroundColor = "orange";
}
*/
$(".top-navigation").click(function(){
	$(".dropdown-menu").addClass("show");
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})


// Price range filter
$( function() {
	$( "#slider-range" ).slider({
		range: true,
		min: 0,
		max: 10000,
		step: 50,
		values: [ 0, 5000 ],
		slide: function( event, ui ) {
			$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		}
	});
	$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) + " - $" + $( "#slider-range" ).slider( "values", 1 ) );
} );
//End Price range filter

function moreFilter() {
	var x = document.getElementById("moreFilter");
	if (x.style.display === "none") {
			x.style.display = "block";
			//x.classList.add("animated");
			//x.classList.add("bounceIn");
	} else {
			x.style.display = "none"; 
	}
}


